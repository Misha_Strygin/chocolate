import java.util.Scanner;


public class Chocolate {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.print("Введите кол-во имеющихся у вас денег: ");
        int money = scanner.nextInt();

        System.out.print("Введите стоимость одной шоколадки: ");
        int price = scanner.nextInt();

        //если денег меньше, чем стоит 1 шоколадка, то мы не сможем купить ни одной шоколадки
        if (money < price) {
            System.out.println("У вас недостаточно денег, чтобы купить одну шоколадку");
            return;
        }

        System.out.print("Какое кол-во обёрток требуется для обмена на дополнительную шоколадку: ");
        int wraps = scanner.nextInt();

        if (wraps <= 1) {
            System.out.println("Вы не можете обменять 0 или 1 обертку на новую шоколадку, это будет происходить бесконечно");
            return;
        }

        //кол-во шоколадок, которые вы можете купить на имеющиеся деньги
        int chocolates = money / price;
        //кол-во обёрток которое вы получите
        int wraps_count = chocolates;

        //пока у вас есть возможность обменивать обёртки на новые шоколадки
        while (wraps_count >= wraps) {
            //отдаёте обёртки в магазин
            wraps_count -= wraps;
            //получаете новую шоколадку
            chocolates++;
            //съедаете шоколадку и получаете новую обёртку
            wraps_count++;
        }

        System.out.printf("Вы сможете купить шоколадок: " + chocolates);
    }
}